$(document).ready(function() {
	$('.submit').click(function(event) {
		
		var error1 = document.getElementById('contact_error1');
		var error2 = document.getElementById('contact_error2');
		var error3 = document.getElementById('contact_error3');
		var email = $('.email').val()
		var imie_nazwisko = $('.imie_nazwisko').val()
		var message = $('.message').val()
		
		
		if(imie_nazwisko.length > 5){
			error1.innerHTML = '';
		}
		else{
			error1.innerHTML = 'Imie i nazwisko są za krótkie';
			event.preventDefault()
		}
		
		
		if(email.length > 5 && email.includes('@') && email.includes('.')) {
			error2.innerHTML = '';
		}
		else{
			error2.innerHTML = 'Email został podany niepoprawnie';
			event.preventDefault()
		}
		
		
		if(message.length > 20){
			error3.innerHTML = '';
		}
		else{
			error3.innerHTML = 'Wiadomość powinna mieć minimum 20 znaków';
			event.preventDefault()
		}
	})
})